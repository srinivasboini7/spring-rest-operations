# Stage 1: Build the application
FROM docker.io/gradle:7.6.1-jdk17 AS build
WORKDIR /app
COPY build.gradle .
COPY src ./src
RUN gradle build --no-daemon
RUN ls -lrt /app/
RUN ls -lrt /app/build/libs/


# Stage 2: Create the final image
FROM docker.io/amazoncorretto:17.0.7-alpine
WORKDIR /app
COPY --from=build /app/build/libs/app-0.0.1-SNAPSHOT.jar .

EXPOSE 8080
CMD ["java", "-jar", "app-0.0.1-SNAPSHOT.jar"]
