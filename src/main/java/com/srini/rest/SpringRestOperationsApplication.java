package com.srini.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Spring rest operations application.
 */
@SpringBootApplication
public class SpringRestOperationsApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringRestOperationsApplication.class, args);
	}

}
