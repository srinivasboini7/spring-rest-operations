package com.srini.rest.controller;

import com.srini.rest.model.Person;
import com.srini.rest.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Random;
import java.util.UUID;

/**
 * The type App controller.
 */
@RestController
@RequestMapping("/app")
@RequiredArgsConstructor
public class AppController {

    private final PersonRepository personRepository ;


    /**
     * Get iterable.
     *
     * @return the iterable
     */
    @GetMapping("/getDefault")
    public Iterable<Person> get(){
       return personRepository.findAll() ;

    }

    /**
     * Get string.
     *
     * @param id the id
     * @return the string
     */
    @GetMapping("/get/{id}")
    public String get(@PathVariable String id){
        return String.format("hello %s !!!", id) ;
    }


    /**
     * Get response entity.
     *
     * @param name the name
     * @param id   the id
     * @return the response entity
     */
    @GetMapping("/get")
    public ResponseEntity<String> get(@RequestParam(name = "name") String name, @RequestParam(name="id") String id){
        return ResponseEntity.ok(String.format("hello %s - %s !!!", name, id)) ;
    }

    /**
     * Save response entity.
     *
     * @param name the name
     * @param age  the age
     * @return the response entity
     */
    @GetMapping("/save/{name}/{age}")
    public ResponseEntity<Person> save(@PathVariable(name = "name") String name, @PathVariable(name = "age") int age){

        Person person = new Person (0,name, age) ;
        personRepository.save(person) ;
        return ResponseEntity.ok(person) ;

    }
}
