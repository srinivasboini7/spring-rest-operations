package com.srini.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The type App controller 2.
 */
@Controller
@ResponseBody
public class AppController2 {


    /**
     * Get string.
     *
     * @return the string
     */
    @RequestMapping("/getDefault")
    public String get(){
        return "hello !!!" ;
    }
}
