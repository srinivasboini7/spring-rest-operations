package com.srini.rest.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;

/**
 * The type Person.
 */
public record Person(@Id int id, String name, int age) {}