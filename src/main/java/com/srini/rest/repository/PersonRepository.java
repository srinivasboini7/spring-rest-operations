package com.srini.rest.repository;

import com.srini.rest.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Person repository.
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, String> {
}
